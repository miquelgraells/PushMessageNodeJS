var express = require('express');
var router = express.Router();
var FCM = require('fcm-node');

/* GET home page. */
router.get('/', function(req, res, next) {
  	res.render('index');	
});

router.post('/sendNotification', function(req, res) {	
	var user = req.body.user;	
	var subject = req.body.subject;	
	var message = req.body.message;	

	req.db.collection('users').findOne({'user': user},function(err,result){        
        if (err) throw err;        
        if(result){
	        var token = result.token;        
	        //sendNotification(subject,message,token);

	    	var serverKey = 'AIzaSyCF1ivVjWcilRQ5_bu3k9O3HPuxNvzdAsI';
			var fcm = new FCM(serverKey);

			var data = {  //you can send only notification or only data(or include both)	       
			    		"subject": subject,
			    		"message": message,
			    	//	"body": message,
					    "title" : subject,
					    "sound" : "default",
					    "priority" : "high"
			    	}

			if (req.body.places)
			    	data.place = req.body.places;

			var pushMessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
			    to: token, 	    	    			    
			    data: data		    
			};

			fcm.send(pushMessage, function(err, response){
			    if (err) {
			        console.log("Something has gone wrong!");
			        res.jsonp({'status':301})
			    } else {
			        console.log("Successfully sent with response: ", response);
			        res.jsonp({'status':200})
			    }
			});    
		} else {
			res.jsonp({'status':301})
		}

    })
});



router.post('/updateUserId', function(req, res) {
	console.log("inside register app");
	var token = req.body.token;
	var user = req.body.user;	
	
	req.db.collection('users').count({'user':user}, function(err, count) {
		// if user dont exists
    	if(count == 0){
    		req.db.collection('users').update({'token':token}, {'token':token , 'user': user},{'upsert':true})
    		res.jsonp({status:200});
    	} else { // user exists
    		res.jsonp({status:301});
    	}
	});	
});

module.exports = router;
