# PushMessageNodeJS
Simple web app : http://pushmessage-mgraells.rhcloud.com/ that allows to sent messages to phones with the app : https://gitlab.com/miquelgraells/PushMessage

![Image](https://gitlab.com/miquelgraells/PushMessageNodeJS/raw/7b975768dbc5b9eb7fda17406c381058dac28f1a/art/1.png)
![Image](https://gitlab.com/miquelgraells/PushMessageNodeJS/raw/7b975768dbc5b9eb7fda17406c381058dac28f1a/art/2.png)
