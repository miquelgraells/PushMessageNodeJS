var App = angular.module('app', []);
App.controller('appController', function controller($scope, $http) {

	// when user closes the modal by clicking outside the modal
	$('#mapModal').on('hidden.bs.modal', function(e){		
	 	$scope.editPlace = false;
	 	$('#address').val("");
	});

	//http://stackoverflow.com/questions/34669955/bootstrap-modal-inside-google-map-is-not-working
	$("#mapModal").on("shown.bs.modal", function () {
	    google.maps.event.trigger(map, "resize");	    
	});

	// to show the result when mesage sent
	$scope.isMessageOK = true;
 	$scope.isMessageSent = false;

 	$scope.editPlace = false; // to know if user is editing
 	$scope.places = new Array(); // all places user saved
 	$scope.placeIndex = 0; // index of place user is editing

 	$scope.showPhoneMap = false; // to show the map on phone preview or not

 	// when users clicks to add place we initizialise the map
 	$scope.initMap = function () {
 		// if there is a previous marker  from other place then we quit that
 		if (marker != null)
			marker.setMap(null);

		// centring map by default in Barcelona. 
		// In the futer try to get the user localtion and set that to cent the map
		map = new google.maps.Map(document.getElementById('map'), {
			center: new google.maps.LatLng(41.3835923, 2.1520363),
			zoom: 13
		});
		// add marker on map where user clicks
		google.maps.event.addListener(map, 'click', function(e) {
			placeMarker(e.latLng);
		});
		// to autocolpit wher user search a place
		geocoder = new google.maps.Geocoder();
		new google.maps.places.Autocomplete(document.getElementById('address'));
 	};

 	// send message to user app 
	$scope.sendMessage = function() {
		var data = {'user':$scope.user,'subject': $scope.subject,'message': $scope.message}
		// if user has added places then we add the array to object data to send
		if ($scope.places[0] != null)
			data.places = $scope.places;
		$http({
			method : "POST",      
			url : "/sendNotification",
			data: data
		}).then(function mySucces(response) {
			// show result message in case of succes
			$scope.isMessageSent = true;
			$scope.isMessageOK = response.data.status == '200' ? true : false;
		});
	};

	// gets place user is looking, ceneters the map and puts a merker the user was looking
	$scope.autoComplitPlace = function() {		
		var address = $('#address').val();
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				if (marker != null) 
					marker.setMap(null);

				map.setCenter(results[0].geometry.location);
				marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
			
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
  		});
	};

	// save new place or modify a previos place
	$scope.savePlace = function() {
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();

		if($scope.editPlace){
			// if the places its modified then whe change the coord of place 
			$scope.places[$scope.placeIndex].lat = lat;
			$scope.places[$scope.placeIndex].lng = lng;
		} else {
			// if its a new place just add the coor to the arry of places			
			$scope.places.push({'lat':lat,'lng':lng});			
		}
	};

	// sets a marker on the map for the coor of place, indicates that the place showed its in edit mode, and indicates
	// the index of place in the array
	$scope.showPlace = function(index) {
		placeMarker($scope.places[index])
		$scope.editPlace = true;
		$scope.placeIndex = index;
	};

	//removes the place that its on edit mode from the array
	$scope.removePlace = function() {				
		$scope.places.splice($scope.placeIndex, 1);		
	}

	// shows tha map on phone preview
	$scope.phoneMap = function(index){		
		var coord = $scope.places[index];

		phoneMap = new google.maps.Map(document.getElementById('phoneMap'), {
			center: new google.maps.LatLng(coord),
			zoom: 13
		});
		$scope.showPhoneMap = true;				

		// to show map once loaded
		// http://stackoverflow.com/questions/832692/how-can-i-check-whether-google-maps-is-fully-loaded
		google.maps.event.addListenerOnce(phoneMap, 'idle', function(){    		
    		google.maps.event.trigger(phoneMap, "resize");
    		// place marker after loading map so the marker shows at center
    		placeMarkerPhoneMap(coord);
		});
	}

});

// ------------------------Marker for modal map ----------------------
var marker = null;

// places a marker on the map given a coord
function placeMarker(position) {
	if (marker != null) 
		marker.setMap(null);        

	marker = new google.maps.Marker({
		position: position,
		map: map
	});  
	map.panTo(position);
}

// ------------------------Marker for phone map ----------------------
var markerPhone = null;

// places a marker on the map given a coord
function placeMarkerPhoneMap(position) {
	if (markerPhone != null) 
		markerPhone.setMap(null);        

	markerPhone = new google.maps.Marker({
		position: position,
		map: phoneMap
	});  
	phoneMap.panTo(position);	
}